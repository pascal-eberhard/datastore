# Data Store

Management tool for different data, e.g. URLs.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg?style=flat-square)](https://php.net/)

## Properties

| Keyword | Value | Comment |
|----|----|----|
| Code Style Check | squizlabs | - |
| Programming language | PHP | - |
| Unit Tests | PHPUnit | - |

## Resources

* [Report issues](https://github.com/PascalEberhardProgramming/DataStore/issues)

## Usage

### Install via composer

````json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://github.com/PascalEberhardProgramming/DataStore.git"
        }
    ],
    "require": {
        "PascalEberhardProgramming/DataStore": ">=1.0.0"
    }
}
````

### PHP

````php
// ...
````
