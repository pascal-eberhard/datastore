<?php

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\FileInfo;

use Symfony\Component\Console\Application;

/**
 * Get file info
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class FileInfo extends Application
{

    /**
     * @param string $name    The name of the application
     * @param string $version The version of the application
     * @see \Symfony\Component\Console\Application::__construct()
     */
    public function __construct($name = 'UNKNOWN', $version = 'UNKNOWN')
    {
        parent::__construct($name, $version);

        $this->addCommands([
            new RunCommand(),
        ]);
    }
}
